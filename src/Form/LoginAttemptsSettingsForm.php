<?php

namespace Drupal\login_attempts\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Login Attempts settings for this site.
 */
class LoginAttemptsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'login_attempts_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['login_attempts.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['login_attempts_threshold'] = [
      '#type' => 'number',
      '#title' => $this->t('Login attempts threshold'),
      '#description' => $this->t('The threshold for showing the warning on the login form. For example: 3 means that the warning will be displayed after the third failed login attempt.'),
      '#default_value' => $this->config('login_attempts.settings')->get('login_attempts_threshold'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('login_attempts.settings')
      ->set('login_attempts_threshold', $form_state->getValue('login_attempts_threshold'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
